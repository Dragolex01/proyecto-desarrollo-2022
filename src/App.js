import { useState, useEffect, useReducer } from 'react' ;
import Menu from "./components/Menu";
import List from "./components/List";
import { initialToursState, toursReducer } from './components/reducer/Tours';
import getAsyncToursList from "./components/ToursList";
import "./styles/App.scss";

function App() {

  const [searchTerm, setSearchTerm] = useState(sessionStorage.getItem('search') != null ? sessionStorage.getItem('search') : '');
  const [toursState, toursDispatch] = useReducer(toursReducer, initialToursState);
  const [toursFavVisualized, setToursFavVisualized] = useState([]);

  useEffect(() => {
    toursDispatch({
        type: 'TOURS_FETCH_INIT'
    })
    getAsyncToursList().then(({tours}) => {
        toursDispatch({
            type: 'TOURS_FETCH_SUCCESS',
            payload: {
                tours
            }
        })
    });
  }, []);

  function setFavTour(tourId, favorite){
    if(toursFavVisualized.includes(tourId)){
        var newFavVisualized = [];
        
        newFavVisualized = toursFavVisualized.filter(item => item !== tourId);
        setToursFavVisualized(newFavVisualized);
    }else{
      setToursFavVisualized([...toursFavVisualized, tourId]);
    }

    toursDispatch({
        type: 'ADD_TOUR_FAVORITE',
        payload: {
            tourId,
            favorite
        }
    })
  }

  function onDelete(id){
    toursDispatch({
        type: 'DELETE_TOUR',
        payload: {
            tours: toursState.tours.filter(item => item.objectID !== id)
        }
    })
  }

  return (
    <div>
      <section className="header">
        <Menu setSearchTerm={setSearchTerm} searchTerm={searchTerm} />
        <div className="header__contenedorTexto">
          <h1 className="header__contenedorTexto--h1">Spain Tours</h1>
          <h2 className="header__contenedorTexto--h2">Tours por España</h2>
        </div>
      </section>
      <section className="sectionTours">
        <List searchTerm={searchTerm} toursState={toursState} onDelete={onDelete} toursFavVisualized={toursFavVisualized} setFavTour={setFavTour} />
      </section>
      <footer className="footer"></footer>
    </div>
  );
}

export default App;
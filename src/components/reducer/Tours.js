const initialToursState = {
    loading: false,
    tours: []
}

const toursReducer = (state, action) => {
    switch (action.type){
        case 'TOURS_FETCH_INIT':
            return{
                ...state,
                loading: true
            }
        case 'TOURS_FETCH_SUCCESS':
            return{
                loading: false,
                tours: action.payload.tours
            }
        case 'ADD_TOUR_FAVORITE':
            return{
                ...state,
                tours: state.tours.map(tour => 
                    tour.objectID === action.payload.tourId
                        ? {...tour, favorite: action.payload.favorite}
                        : {...tour}
                    )
            }
        case 'DELETE_TOUR':
            return{
                tours: action.payload.tours
            }
        default:
            throw Error ('Reducer tours error!');
    }
}

export { initialToursState, toursReducer }
import { useState } from "react";
import Tour from "./Tour";
//import { faBook } from "@fortawesome/free-solid-svg-icons";

//sessionStorage da problemas. Al actualizar el tipo de dato que tiene no es un array. No funciona split(',');

function sortBy(list, key, isReverse){
    const sortedList = list.slice().sort((item1, item2) => {

        if(typeof (item1[key]) === "number"){
            return (item1[key] || 0) - (item2[key] || 0)
        }

        if(item1[key].toLowerCase() < item2[key].toLowerCase()){
            return -1;
        }else if(item1[key].toLowerCase() > item2[key].toLowerCase()){
            return +1;
        }else{
            return 0;
        }
    })
    
    return isReverse ? sortedList.reverse() : sortedList
}

const SORTS = {
    NONE: (list) => list,
    TITLE: (list, isReverse) => sortBy(list, 'title', isReverse),
    PRICE: (list, isReverse) => sortBy(list, 'price', isReverse)
}

function List({ searchTerm, toursState, onDelete, toursFavVisualized, setFavTour }){
    const [mode, setMode] = useState('normal');

    const [sort, setSort] = useState({ sortKey: 'NONE', isReverse: false });
    const sortFunction = SORTS[sort.sortKey];
    const sortedList = sortFunction(toursState.tours, sort.isReverse);
    const handleShort = (sortKey) => { //SortKey --> clave de ordenación
        setSort({ sortKey, isReverse: sortKey === sort.sortKey ? !sort.isReverse : false })
    }

    const maxToursVisualized = 6;
    const listaTours = selectMode();

    //sessionStorage.setItem('toursFav', toursFavVisualized);

    function selectMode(){
        switch (mode){
            case 'normal':
                return(
                    sortedList.filter(tour => tour.title.toLowerCase().includes(searchTerm.toLowerCase())).map((tour, i) => (
                        i < maxToursVisualized
                            ?   <div className="sectionTours__contenedorTours--tour" key={tour.objectID}>
                                    <Tour id={tour.objectID} title={tour.title} info={tour.info} image={tour.image} price={tour.price} toursFavVisualized={toursFavVisualized} setFavTour={setFavTour} handleDelete={onDelete} setFavorite={true} />
                                </div>
                            : null
                    ))
                )
            case 'fav':
                return(
                    sortedList.filter(tour => tour.favorite && tour.title.toLowerCase().includes(searchTerm.toLowerCase())).map((tour, i) => (
                        <div className="sectionTours__contenedorTours--tour" key={tour.objectID}>
                            <Tour id={tour.objectID} title={tour.title} info={tour.info} image={tour.image} price={tour.price} toursFavVisualized={toursFavVisualized} setFavTour={setFavTour} handleDelete={onDelete} setFavorite={false} />
                        </div>
                    ))
                )
            default:
                throw Error("Tours mode error!");
        }
    }

    function personalizar(opcion){
        const botonFiltro = document.getElementById("contenedorFiltro");
        const botonOrdenar = document.getElementById("contenedorOrdenar");

        switch(opcion){
            case 'FILTRAR':
                botonFiltro.style.display === "inline" ? botonFiltro.style.display = "none" : botonFiltro.style.display = "inline";
            
                if(botonOrdenar.style.display === "inline"){
                    botonOrdenar.style.display = "none";
                }
                break;
            
            case 'ORDENAR':
                botonOrdenar.style.display === "inline" ? botonOrdenar.style.display = "none" : botonOrdenar.style.display = "inline";
            
                if(botonFiltro.style.display === "inline"){
                    botonFiltro.style.display = "none";
                }
                break;

            case 'FAVORITO':
                mode === 'normal' ? setMode('fav') : setMode('normal');
                break;

            default:
                throw Error('Error personalizar!');
        }
    }

    return (
        <>
            <h1 className="sectionTours--titulo">Tours</h1>
            <button type="button" className="sectionTours--boton" onClick={() => personalizar('FAVORITO')}>Mostrar {mode === 'normal' ? 'favoritos' : 'todos'}</button>
            <button type="button" className="sectionTours--boton" onClick={() => personalizar('FILTRAR')}>Filtro</button>
            <button type="button" className="sectionTours--boton" onClick={() => personalizar('ORDENAR')}>Ordenar</button>
            <div id="contenedorOrdenar">
                <button type="button" onClick={() => {
                    handleShort('TITLE');
                }} className="sectionTours__contenedorOrdenar--boton">Nombre</button>
                <button type="button" onClick={() => {
                    handleShort('PRICE');
                }} className="sectionTours__contenedorOrdenar--boton">Precio</button>
            </div>
            <div id="contenedorFiltro">
                <button type="button" onClick={() => personalizar('FAVORITO')} className="sectionTours__contenedorFiltros--boton">Favoritos</button>
            </div>
            <div className="sectionTours__contenedorTours">
                {
                    listaTours.length > 0 ? listaTours : <p>NO HAY TOURS EN FAVORITOS</p>
                }
            </div>
        </>
    );
}

export default List;
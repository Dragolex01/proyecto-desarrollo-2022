import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark } from "@fortawesome/free-solid-svg-icons";

function Tour({ id, title, info, image, price, toursFavVisualized, setFavTour, handleDelete, setFavorite }) {

  return (
    <>
      <div className="sectionTours__contenedorTours--contenedorImagen">
        {/* Enlace¿? */}
        <img src={image} className="sectionTours__contenedorTours--imagen" alt={title} />
      </div>
      <button type="button" className="sectionTours__contenedorTours--botonDelete" onClick={() => handleDelete(id)}>Delete</button>
      <FontAwesomeIcon icon={faBookmark} className={toursFavVisualized.includes(id) ? "sectionTours__contenedorTours--iconoFav" : "sectionTours__contenedorTours--icono"}  onClick={() => setFavTour(id, setFavorite)} />
      <h1 className="sectionTours__contenedorTours--titulo">{title}</h1>
      <p className="sectionTours__contenedorTours--precio">{price + "€"}</p>
      <p className="sectionTours__contenedorTours--info">{info.substring(0, 120) + "..."} <a href="#?">Leer mas</a></p> {/* Muestra los primeros 120 carácteres + '...' al final */}
    </>
  )
}

export default Tour;
function Menu({ setSearchTerm, searchTerm }){

    window.addEventListener("scroll", function(){
        var header = document.querySelector('.menu');

        if(window.scrollY > (0.7 * window.screen.height)){
            header.classList.add('menuScrollDown');
        }else{
            header.classList.remove('menuScrollDown');
        }
    })

    function handleSearch(event){
        setSearchTerm(event.target.value);
        sessionStorage.setItem('search', event.target.value);
    }

    return (
        <nav className="menu">
            <ul className="menu__logo--ul  contenedor_logo-ul">
                <li className="menu__logo--li"><a className="menu__link" href="#?">Logo</a></li>
            </ul>
            <div className="menu__buscador">
                <input id="search" type="text" value={searchTerm} className="menu__buscador--barra" placeholder="Empieza tu búsqueda" onChange={handleSearch}/>
            </div>
            <ul className="menu__nav--ul">
                <li className="menu__nav--li"><a className="menu__link" href="#?">Tours</a></li>
                <li className="menu__nav--li"><a className="menu__link" href="#?">Link2</a></li>
                <li className="menu__nav--li"><a className="menu__link" href="#?">Sobre Nosotros</a></li>
            </ul>
        </nav>
    )
}

export default Menu;